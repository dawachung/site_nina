---
title: "Iniciação Científica"
date: 2024-10-10
draft: false
---

- 2024 - presente;
- Tema: Tratamento de dor com o uso de ultrassom;
- Objetivo: Buscar novas tecnologias para tratamento de dor, especificamente com o uso de ultrassom, visando um tratamento pouco invasivo.