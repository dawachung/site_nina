+++
title = 'Currículo'
date = 2024-10-11
draft = false
+++

Educação:
- Ensino médio: Colégio Agostiniano Mendel 
- Ensino superior: Engenharia Mecatrônica na Escola Politécnica de São Paulo (2022-presente)

Indiomas:
- Português: Fluente
- Coreano: Fluente
- Inglês: Avançado

Dados pessoais:
- Email: dawachung@usp.br
- Data de nascimento: 22/11/2002
